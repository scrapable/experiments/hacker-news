export default ({ browser }) => {
    const topPostHandle = await page.$(".athing .storylink")
    const topPostTitle = await page.evaluate(link => link.innerText, topPostHandle)
    const topPostHref = await page.evaluate(link => link.href, topPostHandle)
    await topPostHandle.dispose()

    return {
        timestamp: Date.now(),
        title: topPostTitle,
        link: topPostHref
    }
}
