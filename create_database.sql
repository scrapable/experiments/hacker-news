CREATE TABLE top_posts (
    timestamp DATE PRIMARY KEY,
    title VARCHAR(75),
    link VARCHAR(75)
);
